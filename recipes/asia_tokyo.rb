#
# Cookbook Name:: timezone
# Recipe:: asia_tokyo
#
# Copyright 2012, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

execute "timezone Asia/Tokyo" do
	command 'echo "Asia/Tokyo" > /etc/timezone'
	action :run
end

include_recipe "timezone"