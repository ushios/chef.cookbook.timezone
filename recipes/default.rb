#
# Cookbook Name:: timezone
# Recipe:: default
#
# Copyright 2012, UshioShugo<ushio.s@gmail.com>
#
# All rights reserved - Do Not Redistribute
#

execute "set timezone" do
	command 'dpkg-reconfigure -f noninteractive tzdata'
	action :run
end